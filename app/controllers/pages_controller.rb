class PagesController < ApplicationController
  def welcome
    authorize :page, :welcome?
  end
end

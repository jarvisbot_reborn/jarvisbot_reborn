class SongRequestsController < ApplicationController
  def show
    @song_request = SongRequest.includes(:user, :song).find(params[:id])
    authorize @song_request
  end

  def create
    authorize SongRequest
    @song_request = SongRequest.new(
      song: Song.from_songfinder(params[:request]),
      user: current_user,
      song_queue: SongQueue.find(params[:song_queue])
      )
    if @song_request.save
      flash[:success] = JRR.submitted_successfully("#{@song_request.song.title} by #{@song_request.song.artist}")
    else
      flash[:danger] = @song_request.errors.first
    end
    redirect_back fallback_location: root_url
  end
end

module ExceptionHandler
  extend ActiveSupport::Concern

  included do
    rescue_from Pundit::NotAuthorizedError do |e|
      flash[:danger] = 'Not authorized to perform this action'
      redirect_back fallback_location: root_path
    end

    rescue_from ActiveRecord::RecordNotFound do |e|
      flash[:danger] = "Page not found: #{e.model}:#{e}"
      redirect_back fallback_location: root_url
    end
  end
end
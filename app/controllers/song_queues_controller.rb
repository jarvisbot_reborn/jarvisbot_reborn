class SongQueuesController < ApplicationController
  before_action :authenticate_user!, except: [:show, :latest]

  def show
    @owner = User.friendly.find(params[:id])
    @song_queue = @owner.song_queues.includes(song_requests: [:user, :song])
      .find(params[:song_queue_id])
    authorize @song_queue
  end
  
  def latest
    @owner = User.friendly.find(params[:id])
    @song_queue = @owner.song_queues.includes(song_requests: [:user, :song]).last
    authorize @song_queue || SongQueue
  end

  def create
    if current_user.song_queues.unfinished.any?
      flash.now[:danger] = 'You have an unfinished queue. Finish your old queue to create a new one.'
      authorize SongQueue
    else
      @song_queue = current_user.song_queues.build
      authorize @song_queue
      @song_queue.save
    end
    redirect_to latest_queue_path(current_user)
  end

  def destroy
    @song_queue = SongQueue.find(params[:id])
    authorize @song_queue
    if @song_queue.destroy
      flash[:success] = 'Queue successfully deleted'
    end
    redirect_back fallback_location: root_url
  end
end

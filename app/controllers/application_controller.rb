class ApplicationController < ActionController::Base
  include Pundit
  include ExceptionHandler
  
  protect_from_forgery
  
  after_action :verify_authorized, unless: :devise_controller?

  # no idea if this should be here
  JRR = JarvisbotSongfinder::ReplyMessage::Request
  JRS = JarvisbotSongfinder::ReplyMessage::Song
end

#require 'pry'
class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController

  def twitch
    @user = User.from_omniauth(request.env["omniauth.auth"])
    if @user.persisted?
      sign_in_and_redirect @user
      set_flash_message(:notice, :success, kind: "Twitch") if is_navigational_format?
    else
      session["devise.twitch_data"] = request.env["omniauth.auth"]
      redirect_to root_url
    end
  end

  def failure
    redirect_to root_path
  end
end
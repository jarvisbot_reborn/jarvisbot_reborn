class ConfigsController < ApplicationController
  before_action :authenticate_user!

  def show
    @config = current_user.config
    authorize @config
  end

  def update
    @config = current_user.config
    authorize @config
    if @config.update(config_params)
      flash[:success] = 'Config successfully updated'
    else
      @config.errors.full_messages.each do |m|
        flash[:danger] << m
      end
    end
    redirect_back fallback_location: root_path
  end

  private

  def config_params
    params.require(:config).permit(:region, :length_min, :length_max)
  end
end

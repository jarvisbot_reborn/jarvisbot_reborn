class SongRequestPolicy < ApplicationPolicy
  # Anybody
  def show?
    true
  end

  # Any authenticated user
  def create?
    user
  end

  # Only admin and owner
  def update?
    return true if user&.admin? 
    user && (song_request.user == user)
  end

  # Only admin and owner
  def destroy?
    update?
  end

  # Only admin and queue owner
  # TODO: moderator
  def promote?
    return true if user&.admin?
    user && (song_request&.song_queue&.owner == user)
  end

  # Only admin and queue owner
  # TODO: moderator
  def demote?
    promote?
  end

  # Only admin and queue owner
  def fulfill?
    return true if user&.admin?
    user && (song_request&.song_queue&.owner == user)
  end

  private

  def song_request
    record
  end
end
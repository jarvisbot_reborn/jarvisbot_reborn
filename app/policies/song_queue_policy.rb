class SongQueuePolicy < ApplicationPolicy
  # Anybody
  def show?
    true
  end

  # Anybody
  def latest?
    show?
  end

  # Any authenticated user
  def create?
    user
  end

  # Only admin and owner
  def destroy?
    return true if user&.admin? 
    user && (song_queue.owner == user)
  end

  private

  def song_queue
    record
  end
end
class PagePolicy < Struct.new(:user, :page)

  # Anybody can access the welcome page
  def welcome?
    true
  end
end
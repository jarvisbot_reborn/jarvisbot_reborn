class ConfigPolicy < ApplicationPolicy
  # Owner
  def show?
    user && (config.user == user)
  end

  # Owner
  def update?
    show?
  end

  private 
  def config
    record
  end
end
class UserPolicy < ApplicationPolicy
  # Anybody allowed to view user profile
  def show?
    true
  end
end
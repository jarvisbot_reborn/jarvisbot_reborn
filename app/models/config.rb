class Config < ApplicationRecord
  belongs_to :user
  validates :region, inclusion: { 
    in: IsoCountryCodes::Code.all.map(&:alpha2),
    message: "has to be ISO 3166-1 alpha-2 compliant (two letter country code)" 
  }, presence: true
end

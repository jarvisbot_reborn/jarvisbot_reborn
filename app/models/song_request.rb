class SongRequest < ApplicationRecord
  belongs_to :user
  belongs_to :song
  belongs_to :song_queue

  validates_presence_of :user, :song, :song_queue
  validates_associated  :song, message: lambda{ |class_obj, obj| obj[:value].errors.full_messages.join(",") }

  state_machine :state, initial: :regular do
    audit_trail initial: false, context: :event_performed_by

    event :shuffle do
      transition :regular => :current
    end
    event :promote do
      transition :regular => :promoted
    end
    event :demote do
      transition :promoted => :regular
    end
    event :currentize do
      transition [:regular, :promoted] => :current
    end
    event :fulfill do
      transition :current => :fulfilled
    end
    event :skip do
      transition :current => :skipped
    end
    event :remove do
      transition [:regular, :promoted, :current] => :removed
    end
    event :cancel_fulfillment do
      transition :fulfilled => :regular
    end
  end

  def event_performed_by(transition)
    raise ArgumentError, "Who triggered the evert?" unless transition.args.present?
    transition.args.first
  end
end

class SongQueue < ApplicationRecord
  belongs_to :owner, class_name: :User, foreign_key: :user_id
  has_many :song_requests

  scope :unfinished, -> { where(finished_at: nil) }
end

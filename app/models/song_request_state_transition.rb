class SongRequestStateTransition < ApplicationRecord
  belongs_to :song_request
  belongs_to :user

  def event_performed_by=(user)
    self.user = user
  end
end

class Song < ApplicationRecord
  attr_accessor :songfinder_source

  validates_presence_of   :url, :artist, :title, :length, :provider
  validates_uniqueness_of :url

  validate do |record|
    if record&.songfinder_source&.errors&.any?
      record.errors[:base] << record.songfinder_source.errors.first
    end
  end

  def self.from_songfinder(query)
    if query.match? %r{\Ahttps?:\/\/}
      find_or_initialize_by(url: query) do |song|
        track = JarvisbotSongfinder::Query.new(query)
        song.songfinder_source = track
        song.url      = track.url 
        song.title    = track.title
        song.artist   = track.artist
        song.length   = track.length
        song.provider = track.provider
        song.explicit = track.explicit?
      end
    else
      track = JarvisbotSongfinder::Query.new(query)
      find_or_initialize_by(url: track.url) do |song|
        song.songfinder_source = track
        song.url      = track.url 
        song.title    = track.title
        song.artist   = track.artist
        song.length   = track.length
        song.provider = track.provider
        song.explicit = track.explicit?
      end
    end
  end
end

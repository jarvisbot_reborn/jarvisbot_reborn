class User < ApplicationRecord
  extend FriendlyId
  friendly_id :name, use: :slugged

  devise :omniauthable, omniauth_providers: %i[twitch]

  validates_presence_of :name

  has_many :song_queues
  has_one :config

  after_create :create_config

  def self.from_omniauth(auth)
    if @user = find_by(name: auth.info.nickname, uid: nil)
      @user.update_attributes(uid: auth.uid, provider: auth.provider)
      return @user
    elsif @user = find_by(name: auth.info.nickname, uid: auth.uid, provider: auth.provider)
      @user
    else
      create(name: auth.info.nickname, uid: auth.uid, provider: auth.provider)
    end
  end

  private

  def create_config
    Config.create(user: self)
  end
end

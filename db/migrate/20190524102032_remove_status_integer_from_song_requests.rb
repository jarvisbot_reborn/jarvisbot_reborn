class RemoveStatusIntegerFromSongRequests < ActiveRecord::Migration[5.2]
  def change
    # move rom status to statemachine
    remove_column :song_requests, :status, :integer
  end
end

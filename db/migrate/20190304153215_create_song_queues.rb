class CreateSongQueues < ActiveRecord::Migration[5.2]
  def change
    create_table :song_queues do |t|
      t.references :user, foreign_key: true
      t.datetime :finished_at

      t.timestamps
    end
  end
end

class ChangeColumnDefaultRegionOnConfig < ActiveRecord::Migration[5.2]
  def change
    change_column :configs, :region, :string, default: "US"
    #Ex:- change_column("admin_users", "email", :string, :limit =>25)
  end
end

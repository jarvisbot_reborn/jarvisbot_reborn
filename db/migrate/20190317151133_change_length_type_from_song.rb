class ChangeLengthTypeFromSong < ActiveRecord::Migration[5.2]
  def change
    change_column :songs, :length, 'integer USING CAST(length AS integer)'
  end
end

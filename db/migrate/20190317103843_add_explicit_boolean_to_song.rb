class AddExplicitBooleanToSong < ActiveRecord::Migration[5.2]
  def change
    add_column :songs, :explicit, :boolean, default: false
  end
end

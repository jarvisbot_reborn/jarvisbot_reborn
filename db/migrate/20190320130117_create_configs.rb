class CreateConfigs < ActiveRecord::Migration[5.2]
  def change
    create_table :configs do |t|
      t.string :region
      t.integer :length_min, default: 0
      t.integer :length_max, default: 0
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end

class AddStateStringToSongRequest < ActiveRecord::Migration[5.2]
  def change
    add_column :song_requests, :state, :string
  end
end

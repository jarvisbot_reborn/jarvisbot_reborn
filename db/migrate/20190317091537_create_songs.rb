class CreateSongs < ActiveRecord::Migration[5.2]
  def change
    create_table :songs do |t|
      t.string :url, index: {unique: true}
      t.string :artist
      t.string :title
      t.string :length
      t.string :provider

      t.timestamps
    end
  end
end

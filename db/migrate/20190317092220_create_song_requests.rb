class CreateSongRequests < ActiveRecord::Migration[5.2]
  def change
    create_table :song_requests do |t|
      t.references :user, foreign_key: true
      t.references :song, foreign_key: true
      t.references :song_queue, foreign_key: true
      t.integer :status

      t.timestamps
    end
  end
end

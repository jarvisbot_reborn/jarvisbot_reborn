class CreateSongRequestStateTransitions < ActiveRecord::Migration[5.2]
  def change
    create_table :song_request_state_transitions do |t|
      t.references :song_request, foreign_key: true
      t.string :namespace
      t.string :event
      t.string :from
      t.string :to
      t.timestamp :created_at
      t.references :user, foreign_key: true
    end
  end
end

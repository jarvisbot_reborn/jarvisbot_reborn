# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_05_24_105116) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "configs", force: :cascade do |t|
    t.string "region", default: "US"
    t.integer "length_min", default: 0
    t.integer "length_max", default: 0
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_configs_on_user_id"
  end

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string "slug", null: false
    t.integer "sluggable_id", null: false
    t.string "sluggable_type", limit: 50
    t.string "scope"
    t.datetime "created_at"
    t.index ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true
    t.index ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type"
    t.index ["sluggable_type", "sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_type_and_sluggable_id"
  end

  create_table "song_queues", force: :cascade do |t|
    t.bigint "user_id"
    t.datetime "finished_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_song_queues_on_user_id"
  end

  create_table "song_request_state_transitions", force: :cascade do |t|
    t.bigint "song_request_id"
    t.string "namespace"
    t.string "event"
    t.string "from"
    t.string "to"
    t.datetime "created_at"
    t.bigint "user_id"
    t.index ["song_request_id"], name: "index_song_request_state_transitions_on_song_request_id"
    t.index ["user_id"], name: "index_song_request_state_transitions_on_user_id"
  end

  create_table "song_requests", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "song_id"
    t.bigint "song_queue_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "state"
    t.index ["song_id"], name: "index_song_requests_on_song_id"
    t.index ["song_queue_id"], name: "index_song_requests_on_song_queue_id"
    t.index ["user_id"], name: "index_song_requests_on_user_id"
  end

  create_table "songs", force: :cascade do |t|
    t.string "url"
    t.string "artist"
    t.string "title"
    t.integer "length"
    t.string "provider"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "explicit", default: false
    t.index ["url"], name: "index_songs_on_url", unique: true
  end

  create_table "users", force: :cascade do |t|
    t.string "name", null: false
    t.string "uid"
    t.string "provider"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "slug"
    t.boolean "admin", default: false
    t.index ["slug"], name: "index_users_on_slug", unique: true
  end

  add_foreign_key "configs", "users"
  add_foreign_key "song_queues", "users"
  add_foreign_key "song_request_state_transitions", "song_requests"
  add_foreign_key "song_request_state_transitions", "users"
  add_foreign_key "song_requests", "song_queues"
  add_foreign_key "song_requests", "songs"
  add_foreign_key "song_requests", "users"
end

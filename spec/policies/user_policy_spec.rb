RSpec.describe UserPolicy do
  permissions :show? do
    subject { described_class }
    it "grants permissions to guest" do
      expect(subject).to permit(nil, User.new)
    end
    it "grants permissions to user" do
      expect(subject).to permit(User.new, User.new)
    end
    it "grants permissions to admin" do
      expect(subject).to permit(User.new(admin: true), User.new)
    end
  end
end
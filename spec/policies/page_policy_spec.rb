RSpec.describe PagePolicy do
  subject { described_class }

  permissions :welcome? do
    it "grants permissions to guest" do
      expect(subject).to permit(nil, :welcome?)
    end
    it "grants permissions to user" do
      expect(subject).to permit(User.new, :welcome?)
    end
    it "grants permissions to admin" do
      expect(subject).to permit(User.new(admin: true), :welcome?)
    end
  end
end
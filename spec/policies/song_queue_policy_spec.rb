RSpec.describe SongQueuePolicy do
  subject { described_class }
  let(:klass) { SongQueue }
  permissions :show? do
    it "grants access to guest" do
      expect(subject).to permit(nil, klass.new)
    end
    it "grants access to user" do
      expect(subject).to permit(User.new, klass.new)
    end
    it "grants access to admin" do
      expect(subject).to permit(User.new(admin: true), klass.new)
    end
  end

  permissions :latest? do
    it "grants access to guest" do
      expect(subject).to permit(nil, klass.new)
    end
    it "grants access to user" do
      expect(subject).to permit(User.new, klass.new)
    end
    it "grants access to admin" do
      expect(subject).to permit(User.new(admin: true), klass.new)
    end
  end

  permissions :create? do
    it "denies access to guest" do
      expect(subject).not_to permit(nil, klass.new)
    end
    it "grants access to user" do
      expect(subject).to permit(User.new, klass.new)
    end
    it "grants access to admin" do
      expect(subject).to permit(User.new(admin: true), klass.new)
    end
  end

  permissions :destroy? do
    it "denies access to guest" do
      expect(subject).not_to permit(nil, klass.new)
    end
    it "denies access to user" do
      expect(subject).not_to permit(User.new, klass.new)
    end
    it "grants access to owner" do
      queue = klass.new(owner: User.new)
      expect(subject).to permit(queue.owner, queue)
    end
    it "grants access to admin" do
      expect(subject).to permit(User.new(admin: true), klass.new)
    end
  end
end
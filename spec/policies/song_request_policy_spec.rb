RSpec.describe SongRequestPolicy do
  subject { described_class }
  let(:klass) { SongRequest }

  permissions :show? do
    it "grants access to guest" do
      expect(subject).to permit(nil, klass.new)
    end
    it "grants access to user" do
      expect(subject).to permit(User.new, klass.new)
    end
    it "grants access to admin" do
      expect(subject).to permit(User.new(admin: true), klass.new)
    end
  end

  permissions :create? do
    it "denies access to guest" do
      expect(subject).not_to permit(nil, klass.new)
    end
    it "grants access to user" do
      expect(subject).to permit(User.new, klass.new)
    end
    it "grants access to admin" do
      expect(subject).to permit(User.new(admin: true), klass.new)
    end
  end

  permissions :update? do
    it "denies access to guest" do
      expect(subject).not_to permit(nil, klass.new)
    end
    it "denies access to user" do
      expect(subject).not_to permit(User.new, klass.new)
    end
    it "grants access to owner" do
      song_request = klass.new(user: User.new)
      expect(subject).to permit(song_request.user, song_request)
    end
    it "grants access to admin" do
      expect(subject).to permit(User.new(admin: true), klass.new)
    end
  end

  permissions :destroy? do
    it "denies access to guest" do
      expect(subject).not_to permit(nil, klass.new)
    end
    it "denies access to user" do
      expect(subject).not_to permit(User.new, klass.new)
    end
    it "grants access to owner" do
      song_request = klass.new(user: User.new)
      expect(subject).to permit(song_request.user, song_request)
    end
    it "grants access to admin" do
      expect(subject).to permit(User.new(admin: true), klass.new)
    end
  end

  permissions :promote? do
    it "denies access to guest" do
      expect(subject).not_to permit(nil, klass.new)
    end
    it "denies access to user" do
      expect(subject).not_to permit(User.new, klass.new)
    end
    it "denies access to owner" do
      song_request = klass.new(user: User.new)
      expect(subject).not_to permit(song_request.user, song_request)
    end
    it "grants access to admin" do
      expect(subject).to permit(User.new(admin: true), klass.new)
    end
    it "grants access to queue owner" do
      song_request = create(:song_request)
      expect(subject).to permit(song_request.song_queue.owner, song_request)
    end
  end

  permissions :demote? do
    it "denies access to guest" do
      expect(subject).not_to permit(nil, klass.new)
    end
    it "denies access to user" do
      expect(subject).not_to permit(User.new, klass.new)
    end
    it "denies access to owner" do
      song_request = klass.new(user: User.new)
      expect(subject).not_to permit(song_request.user, song_request)
    end
    it "grants access to admin" do
      expect(subject).to permit(User.new(admin: true), klass.new)
    end
    it "grants access to queue owner" do
      song_request = create(:song_request)
      expect(subject).to permit(song_request.song_queue.owner, song_request)
    end
  end

  permissions :fulfill? do
    it "denies access to guest" do
      expect(subject).not_to permit(nil, klass.new)
    end
    it "denies access to user" do
      expect(subject).not_to permit(User.new, klass.new)
    end
    it "denies access to owner" do
      song_request = klass.new(user: User.new)
      expect(subject).not_to permit(song_request.user, song_request)
    end
    it "grants access to admin" do
      expect(subject).to permit(User.new(admin: true), klass.new)
    end
    it "grants access to queue owner" do
      song_request = create(:song_request)
      expect(subject).to permit(song_request.song_queue.owner, song_request)
    end
  end
end
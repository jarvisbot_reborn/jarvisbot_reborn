require 'rails_helper'

RSpec.describe SongQueuesController, type: :controller do

  describe "GET #show" do
    let(:owner) { create(:user) }
    before { owner.song_queues.create }
    it "returns http success" do
      get :show, params: { id: owner.name, song_queue_id: owner.song_queues.first.id }
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #latest" do
    context "when queue exists" do
      let(:owner) { create(:user) }
      before { owner.song_queues.create }
      it "returns http success" do
        get :latest, params: { id: owner.name }
        expect(response).to have_http_status(:success)
      end
    end
  end

  describe "POST #create" do
    context "when no user" do
      before { post :create }
      it "redirects to login page" do
        assert_redirected_to new_user_session_path
      end
    end
    context "when user authenticated" do
      let(:user) { create(:user) }
      before do
        sign_in user
      end
      context "when user has no unfinished queues" do
        it "creates a queue for logged in user" do
          expect{ post :create }.to change{ user.song_queues.count }
        end
      end
      context "when user has an unfinished queue" do
        before { user.song_queues.create! }
        it "doesn't create the queue" do
          expect{ post :create }.not_to change{ user.song_queues.count }
        end
      end
    end
  end

  describe "DELETE #destroy" do
    context "when no user" do
      before { delete :destroy, params: { id: 1 } }
      it "redirects to login page" do
        assert_redirected_to new_user_session_path
      end
    end
    context "when user authenticated" do
      let(:user) { create(:user) }
      before { sign_in user }
      context "when user owns the queue" do
        let!(:queue) { create(:song_queue, owner: user) }
        it "deletes the queue" do
          expect{ delete :destroy, params: { id: queue.id } }
          .to change{ user.song_queues.count }
        end
      end
      context "when user doesn't own the queue" do
        let(:queue) { create(:song_queue) }
        it "doesn't delete the queue" do
          expect{ delete :destroy, params: { id: queue.id } }
          .not_to change{ user.song_queues.count }
        end
      end
    end
  end
end

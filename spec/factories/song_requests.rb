FactoryBot.define do
  factory :song_request do
    user
    song
    song_queue
  end
end

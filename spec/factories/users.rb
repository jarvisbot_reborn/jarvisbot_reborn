FactoryBot.define do
  factory :user do
    name { Faker::Internet.user_name(nil, '_') }

    trait :admin do
      admin { true }
    end
  end
end

FactoryBot.define do
  factory :song_request_state_transition do
    song_request { nil }
    namespace { "MyString" }
    event { "MyString" }
    from { "MyString" }
    to { "MyString" }
    created_at { "2019-05-24 13:51:16" }
  end
end

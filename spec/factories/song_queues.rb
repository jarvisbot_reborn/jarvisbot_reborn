FactoryBot.define do
  factory :song_queue do
    owner { create(:user) }
    
    trait :finished do
      finished_at { Time.now }
    end
  end
end

FactoryBot.define do
  factory :config do
    region { "MyString" }
    length_min { "MyString" }
    length_max { "MyString" }
    user { nil }
  end
end

FactoryBot.define do
  factory :song do
    url    { Faker::Internet.url }
    artist { Faker::Book.author }
    title  { Faker::Book.title }
    length { rand(200...3000) }
    provider { "spotify" }

    trait :explicit do
      explicit { true }
    end

    trait :youtube do
      provider { "youtube" }
      url { "https://www.youtube.com/watch?v=dQw4w9WgXcQ" }
    end

    trait :spotify do
      url { "https://open.spotify.com/track/1KuWnyoqi9fxu9j1Ow22Cb" }
    end
  end
end

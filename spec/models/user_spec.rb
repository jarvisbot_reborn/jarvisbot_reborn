require 'rails_helper'

RSpec.describe User, type: :model do
  it { should validate_presence_of(:name) }
  it { should have_many(:song_queues) } 
  it { should have_one(:config) }

  describe "config" do 
    let(:user) { create(:user) }
    it "is present after create" do
      expect(user.config).not_to be_nil
    end
  end
  describe "User::from_omniauth" do
    let!(:user) { create(:user) }
    let!(:auth) do
      OmniAuth::AuthHash.new({
        provider: 'twitch',
        uid: '159650864',
        info: {
          nickname: user.name
        } 
      })
    end
    context "when user exists without omniauth attributes defined" do
      before do
        User.from_omniauth(auth)
      end
      it "adds omniauth provider attributes" do
        expect(user.reload.uid).to eq(auth.uid)
        expect(user.reload.provider).to eq(auth.provider)
      end
    end
    context "when user exists with omniauth attributes defined" do
      before { user.update(uid: auth.uid, provider: auth.provider) }
      it "returns user object" do
        expect(User.from_omniauth(auth)).to eq(user)
      end
    end
    context "when user does not exist" do
      let!(:user) { build(:user) }
      it "creates a user" do
        expect{ User.from_omniauth(auth) }.to change{ User.count }.by(1)
        expect(User.first.uid).to eq(auth.uid) 
      end
    end
  end
end

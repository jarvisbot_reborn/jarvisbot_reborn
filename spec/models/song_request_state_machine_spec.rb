require 'rails_helper'

describe SongRequest, type: :model do
  it { is_expected.to have_states :regular, :promoted, :current, :fulfilled, :skipped, :removed }
  
  it { is_expected.to handle_event :shuffle, when: :regular }
  it { is_expected.to handle_event :promote, when: :regular }
  it { is_expected.to handle_event :demote, when: :promoted }
  it { is_expected.to handle_event :currentize, when: :regular }
  it { is_expected.to handle_event :currentize, when: :promoted }
  it { is_expected.to handle_event :fulfill, when: :current }
  it { is_expected.to handle_event :skip, when: :current }
  it { is_expected.to handle_event :remove, when: :regular } 
  it { is_expected.to handle_event :remove, when: :promoted } 
  it { is_expected.to handle_event :remove, when: :current } 
  it { is_expected.to handle_event :cancel_fulfillment, when: :fulfilled }

  context "audit" do
    let(:song_request) { create(:song_request) }
    let(:user) { create(:user) }
    it "creates a log" do
      expect{ song_request.promote(user) }.to change{ SongRequestStateTransition.count }
    end
  end
end
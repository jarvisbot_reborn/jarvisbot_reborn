require 'rails_helper'

RSpec.describe SongRequest, type: :model do
  it { should belong_to(:song) }
  it { should belong_to(:user) }
  it { should belong_to(:song_queue) }

  it { should validate_presence_of(:user) }
  it { should validate_presence_of(:song) }
  it { should validate_presence_of(:song_queue) }
end

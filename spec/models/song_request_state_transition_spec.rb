require 'rails_helper'

RSpec.describe SongRequestStateTransition, type: :model do
  it { should belong_to(:song_request) }
  it { should belong_to(:user) }
end

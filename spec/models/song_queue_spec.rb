require 'rails_helper'

RSpec.describe SongQueue, type: :model do
  it { should belong_to(:owner).class_name(:User) }
  describe "scope unfinished" do
    let(:user) { create(:user) }
    context "when user has unfinished queues" do
      before { user.song_queues.create }
      it "returns true" do
        expect(user.song_queues.unfinished.any?).to be true
      end
    end
    context "when user has no unfinished queues" do
      before { user.song_queues.create(finished_at: Time.now) }
      it "returns false" do
        expect(user.song_queues.unfinished.any?).to be false
      end
    end
    context "when user has no queues" do
      it "returns false" do
        expect(user.song_queues.unfinished.any?).to be false
      end
    end
  end
end

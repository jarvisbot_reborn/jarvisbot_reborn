require 'rails_helper'

RSpec.describe Song, type: :model do
  it { should validate_presence_of(:url) }
  it { should validate_presence_of(:artist) }
  it { should validate_presence_of(:title) }
  it { should validate_presence_of(:length) }
  it { should validate_presence_of(:provider) }
  
  it { should validate_uniqueness_of(:url) }
  
  describe "#from_songfinder" do
    before(:all) do
      JarvisbotSongfinder.configure do |c|
        c.length_max = 600
      end
    end
    after(:all) do
      JarvisbotSongfinder.configure do |c|
        c.length_max = Float::INFINITY
      end
    end
    context "when valid query" do
      let(:query) { "paramore" }
      it "is valid" do
        expect(Song.from_songfinder(query)).to be_valid
      end
    end
    context "when valid url query" do
      let(:query) { "https://open.spotify.com/track/6qV3OEpN6uFCZnzNSslbn1"}
      it "is valid" do
        expect(Song.from_songfinder(query)).to be_valid
      end
    end
    context "when invalid query" do
      let(:query) { "https://open.spotify.com/track/49rhyZLMgZZIsYcAQTnaHS" }
      let(:song) { Song.from_songfinder(query) }
      it "is invalid" do
        expect(song.valid?).to be false
      end
      it "has a proper error" do
        song.validate
        expect(song.errors.full_messages).to include("Your request is too long, please stick to videos not longer than 10 minutes")
      end
    end
  end
end
require 'rails_helper'

RSpec.describe Config, type: :model do
  it { should belong_to(:user) } 
  describe "default" do
    it "region: US" do
      expect(described_class.new.region).to eq "US"
    end
    it "length_min: 0" do
      expect(described_class.new.length_min).to eq 0
    end
    it "length_max: 0" do
      expect(described_class.new.length_max).to eq 0
    end
  end
end

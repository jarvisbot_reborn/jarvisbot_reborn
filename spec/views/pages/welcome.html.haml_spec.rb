require 'rails_helper'

RSpec.describe "pages/welcome.html.haml", type: :view do
  it "says welcome" do
    render
    expect(rendered).to match /Welcome/
  end
end

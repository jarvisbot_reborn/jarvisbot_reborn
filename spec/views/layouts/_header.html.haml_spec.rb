require 'rails_helper'

RSpec.describe "layouts/_header.html.haml", type: :view do
  context "when no user" do
    before do 
      allow(view).to receive(:current_user).and_return(nil)
      render
    end
    it "has login link" do
      expect(rendered).to match /Sign in/
    end
  end

  context "when user logged in" do
    let(:user) { create(:user) }
    before do
      allow(view).to receive(:current_user).and_return(user)
      render
    end
    it "displays username" do
      expect(rendered).to match user.name
    end
  end
end

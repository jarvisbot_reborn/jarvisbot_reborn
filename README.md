# README

## Requirements
# Versions
* ruby 2.5 + (`ruby -v` )
  - using your favorite package manager or tools like `chruby` or `rbenv`
* rails 5.2 + (`rails -v`)
  - `gem install rails`
* bundler 1.17.1 (`bundler -v`)
  - `gem install bundler:1.17.1`

# Daemons
* postgresql with ability for localhost to do whatever or psql user (role?) with `createdb` permissions and setup in `database.yml` accordingly (look at production directive in said file). Better guide at [DigitaOcean](https://www.digitalocean.com/community/tutorials/how-to-setup-ruby-on-rails-with-postgres) (Ignore the RVM part, don't need it if you already have ruby/rails ready)

# Environment
Your shell in which you run `rails server` needs to have those environment variables:
* [Twitch](https://dev.twitch.tv/console/apps) - Register your test app, point callback url to `http://localhost:3000/users/auth/twitch/callback` Or if you changed port (`rails server -p 3005`) - replace it respectively.
  * TWITCH_CLIENT_ID
  * TWITCH_SECRET

* [Spotify](https://developer.spotify.com/dashboard/applications)
  * SPOTIFY_CLIENT_ID
  * SPOTIFY_SECRET (no idea why, but it wants it)

* [Youtube](https://console.developers.google.com/apis/dashboard) You need `YouTube Data API v3` (i think, probably, maybe? Google api's are weird.)
  * YOUTUBE_API_KEY

To set those up you can export them in your `.bashrc` (or `whatever-you-use-rc`) by doing `export TWITCH_CLIENT_ID="yourlongasskey"` or googling what the hell your shell needs to set up an environmental variable, for weird shells.

# Misc
If you want to port forward from the outside internet to your application, you will need to bind rails server to a specific address, or it won't work.
* `rails server --binding=0.0.0.0`

JarvisbotSongfinder.configure do |c|
  c.spotify_client_id = ENV["SPOTIFY_CLIENT_ID"]
  c.spotify_secret    = ENV["SPOTIFY_SECRET"]
  c.youtube_api_key   = ENV["YOUTUBE_API_KEY"]
  c.region            = "US"
end

# TODO: fix needing to manually configure those 
RSpotify.authenticate(JarvisbotSongfinder.configuration.spotify_client_id, JarvisbotSongfinder.configuration.spotify_secret)
Yt.configuration.api_key = JarvisbotSongfinder.configuration.youtube_api_key

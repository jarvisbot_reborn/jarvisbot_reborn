Rails.application.routes.draw do
  root to: 'pages#welcome'

  devise_for :users, controllers: { omniauth_callbacks: 'users/omniauth_callbacks' }
  devise_scope :user do
    get 'sign_in',  :to => 'devise/sessions#new',     :as => :new_user_session
    get 'sign_out', :to => 'devise/sessions#destroy', :as => :destroy_user_session
  end

  scope :u do
    get '/:id',                      to: 'users#show',         as: 'user'
    get '/:id/queue',                to: 'song_queues#latest', as: 'latest_queue'
    get '/:id/queue/:song_queue_id', to: 'song_queues#show',   as: 'song_queue'
  end
  
  scope :queue do
    post   '/',    to: 'song_queues#create',  as: 'create_queue'
    delete '/:id', to: 'song_queues#destroy', as: 'destroy_queue'
  end

  resources :song_requests, only: [:show, :create]
  resources :configs
end

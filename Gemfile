source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '> 2.5.0'

gem 'rails', '~> 5.2.2'
gem 'pg', '>= 0.18', '< 2.0'
gem 'puma', '~> 3.11'
gem 'sass-rails', '~> 5.0'
gem 'uglifier', '>= 1.3.0'

gem 'coffee-rails', '~> 4.2'
gem 'turbolinks', '~> 5'
gem 'jbuilder', '~> 2.5'
gem 'bootsnap', '>= 1.1.0', require: false
gem 'haml-rails', '~> 2.0'

gem 'jarvisbot_songfinder', '~> 1.1.1'

gem 'iso_country_codes'
gem 'irb'

# Users
gem 'devise', '~> 4.6'
gem 'omniauth-twitch', '~> 1.0'
gem 'friendly_id', '~> 5.2.5'

# Rights management
gem "pundit"

# State machine
gem 'state_machines-activerecord'
gem 'state_machines-audit_trail'

group :development, :test do
  gem 'rspec-rails', '~> 3.8'
  gem 'factory_bot_rails', '~> 5.0'
  gem 'shoulda-matchers'
  gem 'pry'
  gem 'vcr'
  gem 'webmock'
  gem 'faker'
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  gem 'state_machines-rspec'
end

group :development do
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

group :test do
  gem 'simplecov', require: false
end
